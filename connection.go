package data

import (
	"bytes"
	"database/sql"
	"encoding/gob"
	"errors"
	"hash/fnv"

	_ "github.com/lib/pq"
)

var ErrNoRows = errors.New("Query returned no rows")
var ErrNoTxStarted = errors.New("No transactions in progress")
var ErrPriorTxIssue = errors.New("Transaction was rolled back due to issues with underlying transactions")

type Connection struct {
	db    database
	stack []transaction
}

// BuildConnection takes in a connection string and builds a wrapped connection from it
func BuildConnection(connection string) (*Connection, error) {
	if d, err := sql.Open("postgres", connection); err != nil {
		return nil, err
	} else {
		db := newDatabase(d)
		return new(db), nil
	}
}

// New takes in a database and builds a wrapped connection with it
func new(db database) *Connection {
	return &Connection{
		db:    db,
		stack: []transaction{},
	}
}

// IsConnected pings the database and returns true if the connection is still active
func (c *Connection) IsConnected() bool {
	err := c.db.Ping()
	return err == nil
}

// PrepareAndExecute performs the actions of Prepare and Execute a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func (c *Connection) PrepareAndExecute(query string, args ...interface{}) (sql.Result, error) {
	if tx := c.peek(); tx != nil {
		return tx.Exec(query, args...)
	}
	return c.db.Exec(query, args...)
}

// PrepareAndQuery performs the actions of Prepare and Query a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func (c *Connection) PrepareAndQuery(query string, args ...interface{}) (*sql.Rows, error) {
	if tx := c.peek(); tx != nil {
		return tx.Query(query, args...)
	}
	return c.db.Query(query, args...)
}

// ExecuteStatements allows the user to execute >= 1 statement in a single sql transaction
func (c *Connection) ExecuteStatements(stmts ...Statement) error {
	if c.peek() == nil {
		c.BeginTx()
		defer c.ExecuteTx()
	}
	tx := c.peek()
	for _, v := range stmts {
		tx.Exec(v.Query, v.Args...)
	}
	return tx.Execute()
}

// Query allows the user to perform a query where >= 1 rows are expected back
// It simply returns the rows so that the user may manipulate the data as they please
func (c *Connection) Query(stmt Statement) (*sql.Rows, error) {
	if tx := c.peek(); tx != nil {
		return tx.Query(stmt.Query, stmt.Args...)
	}
	return c.db.Query(stmt.Query, stmt.Args...)
}

// QueryRow allows the user to perform an sql query where only one row is expected
// The results of the query are put into the passed in data interfaces
func (c *Connection) QueryRow(stmt Statement, data ...interface{}) error {
	row, err := c.Query(stmt)
	if err != nil {
		return err
	}
	defer row.Close()
	if row.Next() {
		err = row.Scan(data...)
	} else {
		err = ErrNoRows
	}
	return err
}

// BeginTx starts a transaction
func (c *Connection) BeginTx() error {
	tx, err := c.db.Begin()
	if err != nil {
		return err
	}
	c.stack = append(c.stack, tx)
	return nil
}

// RollbackTx rolls back the last in progress transaction
func (c *Connection) RollbackTx() error {
	tx, err := c.pop()
	if err != nil {
		return err
	}
	return tx.Rollback()
}

// CommitTx commits the last in progress transaction
func (c *Connection) CommitTx() error {
	tx, err := c.pop()
	if err != nil {
		return err
	}
	return tx.Commit()
}

func (c *Connection) pop() (transaction, error) {
	if len(c.stack) < 1 {
		return nil, ErrNoTxStarted
	}
	tx := c.stack[len(c.stack)-1]
	c.stack = c.stack[:len(c.stack)-1]
	return tx, nil
}

// ExecuteTx determines whether a commit or rollback needs to take place by checking whether any of the components which
// make up the latest transaction produced an error. A commit occurs on no error, otherwise a rollback takes place. The
// boolean value returned is an indicator of whether the transaction attempted to commit
func (c *Connection) ExecuteTx() error {
	if len(c.stack) < 1 {
		return ErrNoTxStarted
	}
	tx := c.stack[len(c.stack)-1]
	c.stack = c.stack[:len(c.stack)-1]
	return tx.Execute()
}

func (c *Connection) peek() transaction {
	if len(c.stack) > 0 {
		return c.stack[len(c.stack)-1]
	}
	return nil
}

// Statement is a struct which represents a prepared statement.
// Calling Prepare allows the user to not have to construct this manually.
type Statement struct {
	Query string
	Args  []interface{}
}

// Hash creates a uint64 representation of the statement
func (s Statement) Hash() uint64 {
	hasher := fnv.New64a()
	hasher.Write([]byte(s.Query))
	for _, arg := range s.Args {
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		err := enc.Encode(arg)
		if err != nil {
			panic(err)
		}
		hasher.Write(buf.Bytes())
	}
	return hasher.Sum64()
}

type tx struct {
	tx     *sql.Tx
	errors []error
}

// Commit commits the transaction
func (t *tx) Commit() error {
	return t.tx.Commit()
}

// Rollback rolls back the transaction
func (t *tx) Rollback() error {
	return t.tx.Rollback()
}

// Execute determines whether a commit or rollback needs to take place by checking whether any of the components which
// make up the transaction produced an error. A commit occurs on no error, otherwise a rollback takes place. The boolean
// value returned is an indicator of whether the transaction attempted to commit
func (t *tx) Execute() error {
	if len(t.errors) > 0 {
		err := t.Rollback()
		if err != nil {
			return err
		}
		return ErrPriorTxIssue
	}
	return t.Commit()
}

func (t *tx) Exec(query string, args ...interface{}) (sql.Result, error) {
	r, err := t.tx.Exec(query, args...)
	return r, t.err(err)
}

// Exec executes a statement
func (t *tx) Query(query string, args ...interface{}) (*sql.Rows, error) {
	r, err := t.tx.Query(query, args...)
	return r, t.err(err)
}

func (t *tx) err(e error) error {
	if e != nil {
		t.errors = append(t.errors, e)
	}
	return e
}

type db struct {
	db *sql.DB
}

func newDatabase(d *sql.DB) database {
	return &db{d}
}

func (d *db) Exec(query string, args ...interface{}) (sql.Result, error) {
	return d.db.Exec(query, args...)
}

func (d *db) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return d.db.Query(query, args...)
}

func (d *db) Ping() error {
	return d.db.Ping()
}

func (d *db) Begin() (transaction, error) {
	dtx, err := d.db.Begin()
	if err != nil {
		return nil, err
	}
	tx := &tx{
		tx:     dtx,
		errors: []error{},
	}
	return tx, nil
}
