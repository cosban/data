package data

import (
	"database/sql"
	"testing"

	"github.com/cosban/assert"
)

func TestHashEquality(t *testing.T) {
	assert := assert.New(t)

	first := Statement{"test", []interface{}{1}}.Hash()
	second := Statement{"test", []interface{}{1}}.Hash()

	assert.NotEquals(0, first)
	assert.Equals(first, second)
}

type mockDatabase struct{}

func (m *mockDatabase) Exec(query string, args ...interface{}) (sql.Result, error) {
	return nil, nil
}

func (m *mockDatabase) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return nil, nil
}

func (m *mockDatabase) Ping() error {
	return nil
}

func (m *mockDatabase) Begin() (transaction, error) {
	return &mockTransaction{}, nil
}

type mockTransaction struct{}

func (m *mockTransaction) Exec(query string, args ...interface{}) (sql.Result, error) {
	return nil, nil
}

func (m *mockTransaction) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return nil, nil
}

func (m *mockTransaction) Commit() error {
	return nil
}

func (m *mockTransaction) Rollback() error {
	return nil
}

func (m *mockTransaction) Execute() error {
	return nil
}
