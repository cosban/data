package data

import "database/sql"

type executor interface {
	Exec(string, ...interface{}) (sql.Result, error)
	Query(string, ...interface{}) (*sql.Rows, error)
}

type database interface {
	executor
	Ping() error
	Begin() (transaction, error)
}

type transaction interface {
	executor
	Commit() error
	Rollback() error
	Execute() error
}
