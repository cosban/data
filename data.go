package data

import (
	"database/sql"

	_ "github.com/lib/pq"
)

var connection *Connection

// IsConnected pings the database and returns true if the connection is still active
func IsConnected() bool {
	return connection.IsConnected()
}

// Connect takes in a valid psql connection string and opens the connection
func Connect(url string) error {
	var err error
	connection, err = BuildConnection(url)
	return err
}

// Prepare creates a Statement which may be used in any of the Execute or Query functions
func Prepare(query string, args ...interface{}) Statement {
	return Statement{query, args}
}

// PrepareAndExecute performs the actions of Prepare and Execute a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func PrepareAndExecute(query string, args ...interface{}) (sql.Result, error) {
	return connection.PrepareAndExecute(query, args...)
}

// PrepareAndQuery performs the actions of Prepare and Query a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func PrepareAndQuery(query string, args ...interface{}) (*sql.Rows, error) {
	return connection.PrepareAndQuery(query, args...)
}

// ExecuteStatements allows the user to execute >= 1 statement in a single sql transaction
func ExecuteStatements(stmts ...Statement) error {
	return connection.ExecuteStatements(stmts...)
}

// Query allows the user to perform a query where >= 1 rows are expected back
// It simply returns the rows so that the user may manipulate the data as they please
func Query(stmt Statement) (*sql.Rows, error) {
	return connection.Query(stmt)
}

// QueryRow allows the user to perform an sql query where only one row is expected
// The results of the query are put into the passed in data interfaces
func QueryRow(stmt Statement, data ...interface{}) error {
	return connection.QueryRow(stmt, data...)
}

// BeginTx starts a transaction
func BeginTx() error {
	return connection.BeginTx()
}

// RollbackTx rolls back the last in progress transaction
func RollbackTx() error {
	return connection.RollbackTx()
}

// CommitTx commits the last in progress transaction
func CommitTx() error {
	return connection.CommitTx()
}

// ExecuteTx determines whether a commit or rollback needs to take place by checking whether any of the components which
// make up the latest transaction produced an error. A commit occurs on no error, otherwise a rollback takes place
func ExecuteTx() error {
	return connection.ExecuteTx()
}
